package com.bca.bsi.smartbot.dao;

import com.bca.bsi.smartbot.model.response.ResponseMessage;

public interface SmartbotDao {
    public ResponseMessage generateBundle(String clientID, String hashcode, String noRekening, String profilResiko);
    public ResponseMessage generateCustomBundle(String clientID, String hashcode, String noRekening, String[] reksaDanaID, String[] persentase);
}
