package com.bca.bsi.smartbot.dao;

import com.bca.bsi.mvasolver.matrix.model.Matrix;
import com.bca.bsi.mvasolver.mva.model.AssetIndexAndPortfolio;
import com.bca.bsi.mvasolver.mva.model.Portfolio;
import com.bca.bsi.mvasolver.robo.Robo;
import com.bca.bsi.smartbot.client.MainframeClient;
import com.bca.bsi.smartbot.model.ReksadanaPerformance;
import com.bca.bsi.smartbot.model.response.*;
import com.bca.bsi.util.ClientIDValidation;
import com.bca.bsi.util.Constant;
import com.bca.bsi.util.ErrorMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Repository
public class SmartbotDaoImpl implements SmartbotDao {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public ResponseMessage generateBundle(String clientID, String hashcode, String noRekening, String profilResiko) {
        ErrorSchema errorSchema = new ErrorSchema();
        OutputSchema outputSchema = new OutputSchema();
        List<Bundle> bundleList = new ArrayList<>();
        Bundle bundle = new Bundle();
        List<Bundle.Reksadana> reksadanaList = new ArrayList<>();
        ResponseMessage responseMessage = new ResponseMessage();


        String query = "SELECT performance_product_reksa_dana.reksa_dana_id AS \"reksaDanaID\", " +
                "performance_product_reksa_dana.closing_price AS \"closingPrice\" " +
                "FROM PERFORMANCE_PRODUCT_REKSA_DANA " +
                "INNER JOIN eai_reksa_dana " +
                "ON performance_product_reksa_dana.reksa_dana_id = eai_reksa_dana.reksa_dana_id ";

        switch (profilResiko) {
            case "1":
                query += "WHERE eai_reksa_dana.product_category_id = 1";
                break;
            case "2":
                query += "WHERE eai_reksa_dana.product_category_id IN (1,2,5)";
                break;
            case "3":
                query += "WHERE eai_reksa_dana.product_category_id IN (1,2,3,5)";
                break;
            default: break;
        }

        query += " ORDER BY performance_product_reksa_dana.reksa_dana_id, to_number(performance_product_reksa_dana.bulan) asc";

        List<ReksadanaPerformance> reksadanaPerformanceList = jdbcTemplate.query(query, new BeanPropertyRowMapper<>(ReksadanaPerformance.class));

        double[][]assetPrices = new double[reksadanaPerformanceList.size()/12][12];

        for (int i = 0; i < reksadanaPerformanceList.size(); i++) {
            assetPrices[i/12][i%12] = reksadanaPerformanceList.get(i).getClosingPrice();
        }

        Robo robo = new Robo(assetPrices, 3);
        AssetIndexAndPortfolio bundle1 = robo.getBundle1();
        AssetIndexAndPortfolio bundle2 = robo.getBundle2();

        int[] assetIndexBundle1 = bundle1.getAssetIndex();
        int[] assetIndexBundle2 = bundle2.getAssetIndex();

        String[] resultBundle1 = bundle1.getPortfolio().getResults();
        String[] resultBundle2 = bundle2.getPortfolio().getResults();

        setReksaDanaBundle(clientID, noRekening, reksadanaList, reksadanaPerformanceList, resultBundle1, assetIndexBundle1);

        bundle.setReksadanaList(reksadanaList);
        bundle.setReturnExpectation(resultBundle1[0]);
        bundle.setRiskExpectation(resultBundle1[1]);
        bundle.setMinimumPurchase(getMinimumBundlePurchase(reksadanaList));
        bundleList.add(bundle);

        Bundle bundles = new Bundle();
        List<Bundle.Reksadana> reksadanaList2 = new ArrayList<>();
        setReksaDanaBundle(clientID, noRekening, reksadanaList2, reksadanaPerformanceList, resultBundle2, assetIndexBundle2);

        bundles.setReksadanaList(reksadanaList2);
        bundles.setReturnExpectation(resultBundle2[0]);
        bundles.setRiskExpectation(resultBundle2[1]);
        bundles.setMinimumPurchase(getMinimumBundlePurchase(reksadanaList2));
        bundleList.add(bundles);

        outputSchema.setBundle(bundleList);

        errorSchema.setErrorCode(ErrorMapping.SUCCESS_ERROR_CODE);
        errorSchema.setErrorMessage(ErrorMapping.SUCCESS_ERROR_MESSAGE_INDONESIAN);

        responseMessage.setErrorSchema(errorSchema);
        responseMessage.setOutputSchema(outputSchema);

        return responseMessage;
    }

    @Override
    public ResponseMessage generateCustomBundle(String clientID, String hashcode, String noRekening, String[] reksaDanaID, String[] persentase) {
        ErrorSchema errorSchema = new ErrorSchema();
        OutputSchema outputSchema = new OutputSchema();
        List<Bundle> bundleList = new ArrayList<>();
        Bundle bundle = new Bundle();
        List<Bundle.Reksadana> reksadanaList = new ArrayList<>();
        ResponseMessage responseMessage = new ResponseMessage();
        Robo robo;
        Portfolio portfolio;

//        if (!ClientIDValidation.isAllowed(jdbcTemplate, hashcode, clientID)) {
//            errorSchema.setErrorCode(ErrorMapping.FORBIDDEN);
//            errorSchema.setErrorMessage(ErrorMapping.FORBIDDEN_ERROR_MESSAGE_INDONESIAN);
//            responseMessage.setErrorSchema(errorSchema);
//            return responseMessage;
//        }

        String query = "SELECT reksa_dana_id AS \"reksaDanaID\"," +
                " closing_price AS \"closingPrice\" FROM PERFORMANCE_PRODUCT_REKSA_DANA WHERE reksa_dana_id = " + reksaDanaID[0];

        for (int i = 1; i < reksaDanaID.length; i++) {
            query += " OR reksa_dana_id = " + reksaDanaID[i];
        }

        query += " ORDER BY reksa_dana_id, to_number(bulan) asc";

        List<ReksadanaPerformance> reksadanaPerformanceList = jdbcTemplate.query(query, new BeanPropertyRowMapper<>(ReksadanaPerformance.class));

        double[][] selectedAssetPrices = new double[reksaDanaID.length][12];

        for (int i = 0; i < reksadanaPerformanceList.size(); i++) {
            selectedAssetPrices[i/12][i%12] = reksadanaPerformanceList.get(i).getClosingPrice();
        }

        if (persentase.length == 0) {
            robo = new Robo();
            portfolio = robo.getComputedCustomPortfolio(selectedAssetPrices);
        } else {
            double[] parsedPersentase =new double[persentase.length];
            for (int i = 0; i < persentase.length; i++) {
                parsedPersentase[i] = Double.parseDouble(persentase[i]);
            }
            robo = new Robo();
            portfolio = robo.getCustomPortfolio(selectedAssetPrices, parsedPersentase);
        }

        String[] rawResult = portfolio.getResults();
        String[] result = formatResult(rawResult);

        String[] proportion = portfolio.getPortfolioProportionString();

        int[] assetIndex = getAssetIndexFromProportion(proportion);

        setReksaDanaBundle(clientID, noRekening, reksadanaList, reksadanaPerformanceList, result, assetIndex);

        bundle.setReksadanaList(reksadanaList);
        bundle.setReturnExpectation(result[0]);
        bundle.setRiskExpectation(result[1]);
        bundle.setMinimumPurchase(getMinimumBundlePurchase(reksadanaList));
        bundleList.add(bundle);

        errorSchema.setErrorCode(ErrorMapping.SUCCESS_ERROR_CODE);
        errorSchema.setErrorMessage(ErrorMapping.SUCCESS_ERROR_MESSAGE_INDONESIAN);

        outputSchema.setBundle(bundleList);

        responseMessage.setErrorSchema(errorSchema);
        responseMessage.setOutputSchema(outputSchema);

        return responseMessage;
    }

    private void setReksaDanaBundle(String clientID,
                                    String noRekening,
                                    List<Bundle.Reksadana> reksadanaList,
                                    List<ReksadanaPerformance> reksadanaPerformanceList,
                                    String[] resultBundle,
                                    int[] assetIndexBundle) {
        String query;

        for (int i = 0; i < assetIndexBundle.length; i++) {
            int index = assetIndexBundle[i] * 12;

//            if (MainframeClient.isFirstPurchase(clientID, noRekening, String.valueOf(reksadanaPerformanceList.get(assetIndexBundle[i]).getReksaDanaID()))) {
//                query = "SELECT name as \"namaReksadana\",\n" +
//                        "product_category as \"jenisReksadana\",\n" +
//                        "kinerja_1_bulan as \"kinerjaTerakhir\",\n" +
//                        "nab_unit as \"nab\",\n" +
//                        "update_date as \"updateDate\",\n" +
//                        "minimum_pembelian_pertama as \"minimumPembelian\"\n" +
//                        "FROM eai_reksa_dana where reksa_dana_id = " + reksadanaPerformanceList.get(index).getReksaDanaID();
//
//            } else {
//                query = "SELECT name as \"namaReksadana\",\n" +
//                        "product_category as \"jenisReksadana\",\n" +
//                        "kinerja_1_bulan as \"kinerjaTerakhir\",\n" +
//                        "nab_unit as \"nab\",\n" +
//                        "update_date as \"updateDate\",\n" +
//                        "minimum_pembelian_selanjutnya as \"minimumPembelian\"\n" +
//                        "FROM eai_reksa_dana where reksa_dana_id = " + reksadanaPerformanceList.get(index).getReksaDanaID();
//            }

            query = "SELECT name as \"namaReksadana\",\n" +
                    "product_category as \"jenisReksadana\",\n" +
                    "kinerja_1_bulan as \"kinerjaTerakhir\",\n" +
                    "nab_unit as \"nab\",\n" +
                    "update_date as \"updateDate\",\n" +
                    "minimum_pembelian_pertama as \"minimumPembelian\"\n" +
                    "FROM eai_reksa_dana where reksa_dana_id = " + reksadanaPerformanceList.get(index).getReksaDanaID();

            ReksadanaData reksadanaData = jdbcTemplate.queryForObject(query, new BeanPropertyRowMapper<>(ReksadanaData.class));

            Bundle.Reksadana reksadana = new Bundle().new Reksadana();
            reksadana.setPersentase(resultBundle[i+2]);
            reksadana.setJenisReksadana(reksadanaData.getJenisReksadana());
            reksadana.setNamaReksadana(reksadanaData.getNamaReksadana());
            reksadana.setKinerjaTerakhir(reksadanaData.getKinerjaTerakhir());
            reksadana.setNab(reksadanaData.getNab());
            reksadana.setMinimumPembelian(reksadanaData.getMinimumPembelian());
            reksadana.setUpdateDate(reksadanaData.getUpdateDate());

            reksadanaList.add(reksadana);
        }
   }

    private int[] getAssetIndexFromProportion(String[] proportion) {
        ArrayList<Integer> assetIndex = new ArrayList<>();
        for (int i = 0; i < proportion.length; i++) {
            if (!proportion[i].equalsIgnoreCase("0%")) {
                assetIndex.add(i);
            }
        }
        int[] assetIndexArr = new int[assetIndex.size()];
        for (int i = 0; i < assetIndex.size(); i++) {
            assetIndexArr[i] = assetIndex.get(i);
        }

        return assetIndexArr;
    }

    private String[] formatResult(String[] raw) {
        String[] formatted;
        int counter = 0;
        for (int i = 2; i < raw.length; i++) {
            if (!raw[i].equalsIgnoreCase("0%")) {
                counter++;
            }
        }

        formatted = new String[counter+2];

        counter = 0;
        for (int i = 0; i < raw.length; i++) {
            if (i < 2) {
                formatted[counter] = raw[i];
                counter++;
            } else {
                if (!raw[i].equalsIgnoreCase("0%")) {
                    formatted[counter] = raw[i];
                    counter++;
                }
            }
        }

        return formatted;
    }

    private String getMinimumBundlePurchase(List<Bundle.Reksadana> reksadanaList) {
        double max = 0;
        double[] minimumPurchase = new double[reksadanaList.size()];
        for (int i = 0; i < minimumPurchase.length; i++) {
            minimumPurchase[i] = Double.parseDouble(reksadanaList.get(i).getMinimumPembelian()) / (0.01 * Double.parseDouble(reksadanaList.get(i).getPersentase().substring(0, reksadanaList.get(i).getPersentase().length() - 1)));
        }
        for (double d : minimumPurchase) {
            if (d > max) {
                max = d;
            }
        }
        return String.valueOf(max);
    }
}
