package com.bca.bsi.smartbot.client.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MFResponse {
    @JsonProperty("BITAPI03")
    private MFResponse.BITAPI03 bitapi03;

    public MFResponse.BITAPI03 getBitapi03() {
        return bitapi03;
    }

    public void setBitapi03(MFResponse.BITAPI03 bitapi03) {
        this.bitapi03 = bitapi03;
    }

    public class BITAPI03 {
        @JsonProperty("WS_FILEIN_REC")
        private MFResponse.BITAPI03.FileIn fileIn;

        @JsonProperty("WS_OUTFILE_REC")
        private MFResponse.BITAPI03.FileOut fileOut;

        public MFResponse.BITAPI03.FileIn getFileIn() {
            return fileIn;
        }

        public void setFileIn(MFResponse.BITAPI03.FileIn fileIn) {
            this.fileIn = fileIn;
        }

        public MFResponse.BITAPI03.FileOut getFileOut() {
            return fileOut;
        }

        public void setFileOut(MFResponse.BITAPI03.FileOut fileOut) {
            this.fileOut = fileOut;
        }

        public class FileIn {
            @JsonProperty("WS_FILEIN_TRANS_ID")
            private String transactionID;

            @JsonProperty("WS_FILEIN_FLAG")
            private String flag;

            @JsonProperty("WS_FILEIN_RDN_NO")
            private int reksaDanaID;

            @JsonProperty("WS_FILEIN_BCAID")
            private String bcaID;

            @JsonProperty("WS_FILEIN_CLIENT_ID")
            private String clientID;

            @JsonProperty("WS_FILEIN_ACCT_NO")
            private String noRekening;

            public String getNoRekening() {
                return noRekening;
            }

            public void setNoRekening(String noRekening) {
                this.noRekening = noRekening;
            }

            public String getTransactionID() {
                return transactionID;
            }

            public void setTransactionID(String transactionID) {
                this.transactionID = transactionID;
            }

            public String getClientID() {
                return clientID;
            }

            public void setClientID(String clientID) {
                this.clientID = clientID;
            }

            public String getBcaID() {
                return bcaID;
            }

            public void setBcaID(String bcaID) {
                this.bcaID = bcaID;
            }

            public String getFlag() {
                return flag;
            }

            public void setFlag(String flag) {
                this.flag = flag;
            }

            public int getReksaDanaID() {
                return reksaDanaID;
            }

            public void setReksaDanaID(int reksaDanaID) {
                this.reksaDanaID = reksaDanaID;
            }
        }

        public class FileOut {
            @JsonProperty("WS_OUTFILE_MESSAGE")
            String message;

            @JsonProperty("WS_OUTFILE_CODE")
            int responseCode;

            @JsonProperty("WS_OUTFILE_RESULT")
            String result;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public int getResponseCode() {
                return responseCode;
            }

            public void setResponseCode(int responseCode) {
                this.responseCode = responseCode;
            }

            public String getResult() {
                return result;
            }

            public void setResult(String result) {
                this.result = result;
            }
        }
    }
}
