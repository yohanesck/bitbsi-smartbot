package com.bca.bsi.smartbot.client.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MFRequest {
    @JsonProperty("BITAPI03")
    private BITAPI03 bitapi03;

    public BITAPI03 getBitapi03() {
        return bitapi03;
    }

    public void setBitapi03(BITAPI03 bitapi03) {
        this.bitapi03 = bitapi03;
    }

    public class BITAPI03 {
        @JsonProperty("WS_FILEIN_REC")
        private FileIn fileIn;

        public FileIn getFileIn() {
            return fileIn;
        }

        public void setFileIn(FileIn fileIn) {
            this.fileIn = fileIn;
        }

        public class FileIn {
            @JsonProperty("WS_FILEIN_TRANS_ID")
            private String transactionID;

            @JsonProperty("WS_FILEIN_FLAG")
            private String flag;

            @JsonProperty("WS_FILEIN_RDN_NO")
            private int reksaDanaID;

            @JsonProperty("WS_FILEIN_BCAID")
            private String bcaID;

            @JsonProperty("WS_FILEIN_CLIENT_ID")
            private String clientID;

            @JsonProperty("WS_FILEIN_ACCT_NO")
            private String noRekening;

            public String getNoRekening() {
                return noRekening;
            }

            public void setNoRekening(String noRekening) {
                this.noRekening = noRekening;
            }

            public String getTransactionID() {
                return transactionID;
            }

            public void setTransactionID(String transactionID) {
                this.transactionID = transactionID;
            }

            public String getClientID() {
                return clientID;
            }

            public void setClientID(String clientID) {
                this.clientID = clientID;
            }

            public String getBcaID() {
                return bcaID;
            }

            public void setBcaID(String bcaID) {
                this.bcaID = bcaID;
            }

            public String getFlag() {
                return flag;
            }

            public void setFlag(String flag) {
                this.flag = flag;
            }

            public int getReksaDanaID() {
                return reksaDanaID;
            }

            public void setReksaDanaID(int reksaDanaID) {
                this.reksaDanaID = reksaDanaID;
            }
        }
    }
}
