package com.bca.bsi.smartbot.client;

import com.bca.bsi.smartbot.client.request.MFRequest;
import com.bca.bsi.smartbot.client.response.MFResponse;
import org.springframework.web.client.RestTemplate;

public class MainframeClient {
    public static boolean isFirstPurchase(String clientID, String noRekening, String reksaDanaID) {
        final String uri = "http://10.20.218.9:9083/project_zh01/";

        MFRequest mfRequest = new MFRequest();
        MFRequest.BITAPI03 bitapi03 = mfRequest.new BITAPI03();
        MFRequest.BITAPI03.FileIn fileIn = bitapi03.new FileIn();

        fileIn.setTransactionID("ZH01");
        fileIn.setClientID(clientID);
        fileIn.setBcaID("");
        fileIn.setFlag("F");
        fileIn.setNoRekening(noRekening);
        fileIn.setReksaDanaID(Integer.parseInt(reksaDanaID));

        bitapi03.setFileIn(fileIn);
        mfRequest.setBitapi03(bitapi03);

        RestTemplate restTemplate = new RestTemplate();

        MFResponse mfResponse = restTemplate.postForObject(uri, mfRequest, MFResponse.class);

        return mfResponse.getBitapi03().getFileOut().getResult().equalsIgnoreCase("true");
    }
}
