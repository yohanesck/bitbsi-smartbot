package com.bca.bsi.smartbot.services;

import com.bca.bsi.smartbot.model.response.ResponseMessage;

public interface SmartbotService {
    public ResponseMessage generateBundle(String clientID, String hashcode, String noRekening, String profilResiko);
    public ResponseMessage generateCustomBundle(String clientID, String hashcode, String noRekening, String[] reksaDanaID, String[] persentase);
}
