package com.bca.bsi.smartbot.services;

import com.bca.bsi.smartbot.dao.SmartbotDao;
import com.bca.bsi.smartbot.model.response.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Resource
public class SmarbotServiceImpl implements SmartbotService{
    @Autowired
    SmartbotDao smartbotDao;

    @Override
    public ResponseMessage generateBundle(String clientID, String hashcode, String noRekening, String profilResiko) {
        return smartbotDao.generateBundle(clientID, hashcode, noRekening, profilResiko);
    }

    @Override
    public ResponseMessage generateCustomBundle(String clientID, String hashcode, String noRekening, String[] reksaDanaID, String[] persentase) {
        return smartbotDao.generateCustomBundle(clientID, hashcode, noRekening, reksaDanaID, persentase);
    }
}
