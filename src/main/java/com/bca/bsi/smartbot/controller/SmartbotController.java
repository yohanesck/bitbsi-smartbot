package com.bca.bsi.smartbot.controller;

import com.bca.bsi.smartbot.model.response.ResponseMessage;
import com.bca.bsi.smartbot.services.SmartbotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
public class SmartbotController {
    @Autowired
    SmartbotService smartbotService;

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseMessage> generateBundle(@RequestHeader(value = "client_id") String clientID,
                                                          @RequestHeader(value = "hashcode") String hashcode,
                                                          @RequestHeader(value = "no_rekening") String noRekening,
                                                          @RequestHeader(value = "profil_resiko") String profilResiko) {
        ResponseMessage result = smartbotService.generateBundle(clientID, hashcode, noRekening, profilResiko);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/custom", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseMessage> generateCustomBundle(@RequestHeader(value = "client_id") String clientID,
                                                                @RequestHeader(value = "hashcode") String hashcode,
                                                                @RequestHeader(value = "no_rekening") String noRekening,
                                                                @RequestParam(value = "reksa-dana-id") String[] reksaDanaID,
                                                                @RequestParam(value = "proportion") String[] persentase) {
        ResponseMessage result = smartbotService.generateCustomBundle(clientID, hashcode, noRekening, reksaDanaID, persentase);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
