package com.bca.bsi.smartbot.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReksadanaPerformance {
    private int reksaDanaID;
    private double closingPrice;

    public int getReksaDanaID() {
        return reksaDanaID;
    }

    public void setReksaDanaID(int reksaDanaID) {
        this.reksaDanaID = reksaDanaID;
    }

    public double getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(double closingPrice) {
        this.closingPrice = closingPrice;
    }
}
