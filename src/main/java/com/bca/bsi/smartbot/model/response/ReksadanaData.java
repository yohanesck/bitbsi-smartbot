package com.bca.bsi.smartbot.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReksadanaData {
    @JsonProperty("nama_reksa_dana")
    private String namaReksadana;

    @JsonProperty("jenis_reksa_dana")
    private String jenisReksadana;

    @JsonProperty("kinerja_terakhir")
    private String kinerjaTerakhir;

    @JsonProperty("nab")
    private String nab;

    @JsonProperty("update_date")
    private String updateDate;

    @JsonProperty("minimum_pembelian")
    private String minimumPembelian;

    public String getNamaReksadana() {
        return namaReksadana;
    }

    public void setNamaReksadana(String namaReksadana) {
        this.namaReksadana = namaReksadana;
    }

    public String getJenisReksadana() {
        return jenisReksadana;
    }

    public void setJenisReksadana(String jenisReksadana) {
        this.jenisReksadana = jenisReksadana;
    }

    public String getKinerjaTerakhir() {
        return kinerjaTerakhir;
    }

    public void setKinerjaTerakhir(String kinerjaTerakhir) {
        this.kinerjaTerakhir = kinerjaTerakhir;
    }

    public String getNab() {
        return nab;
    }

    public void setNab(String nab) {
        this.nab = nab;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getMinimumPembelian() {
        return minimumPembelian;
    }

    public void setMinimumPembelian(String minimumPembelian) {
        this.minimumPembelian = minimumPembelian;
    }
}
