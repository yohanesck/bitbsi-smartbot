package com.bca.bsi.smartbot.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OutputSchema {
    @JsonProperty("bundles")
    private List<Bundle> bundle;

    public List<Bundle> getBundle() {
        return bundle;
    }

    public void setBundle(List<Bundle> bundle) {
        this.bundle = bundle;
    }
}
