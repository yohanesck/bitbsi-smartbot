package com.bca.bsi.smartbot.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Bundle {
    @JsonProperty("reksa_dana_list")
    private List<Reksadana> reksadanaList;

    @JsonProperty("minimum_purchase")
    private String minimumPurchase;

    @JsonProperty("return")
    private String returnExpectation;

    @JsonProperty("risk")
    private String riskExpectation;

    public String getMinimumPurchase() {
        return minimumPurchase;
    }

    public void setMinimumPurchase(String minimumPurchase) {
        this.minimumPurchase = minimumPurchase;
    }

    public List<Reksadana> getReksadanaList() {
        return reksadanaList;
    }

    public void setReksadanaList(List<Reksadana> reksadanaList) {
        this.reksadanaList = reksadanaList;
    }

    public String getReturnExpectation() {
        return returnExpectation;
    }

    public void setReturnExpectation(String returnExpectation) {
        this.returnExpectation = returnExpectation;
    }

    public String getRiskExpectation() {
        return riskExpectation;
    }

    public void setRiskExpectation(String riskExpectation) {
        this.riskExpectation = riskExpectation;
    }

    public class Reksadana {
        @JsonProperty("nama_reksa_dana")
        private String namaReksadana;

        @JsonProperty("jenis_reksa_dana")
        private String jenisReksadana;

        @JsonProperty("kinerja_terakhir")
        private String kinerjaTerakhir;

        @JsonProperty("nab")
        private String nab;

        @JsonProperty("update_date")
        private String updateDate;

        @JsonProperty("minimum_pembelian")
        private String minimumPembelian;

        @JsonProperty("persentase")
        private String persentase;

        public String getNamaReksadana() {
            return namaReksadana;
        }

        public void setNamaReksadana(String namaReksadana) {
            this.namaReksadana = namaReksadana;
        }

        public String getJenisReksadana() {
            return jenisReksadana;
        }

        public void setJenisReksadana(String jenisReksadana) {
            this.jenisReksadana = jenisReksadana;
        }

        public String getKinerjaTerakhir() {
            return kinerjaTerakhir;
        }

        public void setKinerjaTerakhir(String kinerjaTerakhir) {
            this.kinerjaTerakhir = kinerjaTerakhir;
        }

        public String getNab() {
            return nab;
        }

        public void setNab(String nab) {
            this.nab = nab;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getMinimumPembelian() {
            return minimumPembelian;
        }

        public void setMinimumPembelian(String minimumPembelian) {
            this.minimumPembelian = minimumPembelian;
        }

        public String getPersentase() {
            return persentase;
        }

        public void setPersentase(String persentase) {
            this.persentase = persentase;
        }
    }

}
