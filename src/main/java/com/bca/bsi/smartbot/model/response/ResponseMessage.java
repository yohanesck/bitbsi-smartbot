package com.bca.bsi.smartbot.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseMessage {
    @JsonProperty("error_schema")
    private ErrorSchema errorSchema;

    @JsonProperty("output_schema")
    private OutputSchema outputSchema;

    public ErrorSchema getErrorSchema() {
        return errorSchema;
    }

    public void setErrorSchema(ErrorSchema errorSchema) {
        this.errorSchema = errorSchema;
    }

    public OutputSchema getOutputSchema() {
        return outputSchema;
    }

    public void setOutputSchema(OutputSchema outputSchema) {
        this.outputSchema = outputSchema;
    }
}
